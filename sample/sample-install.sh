#!/bin/bash
insdir=/var/lmr/pkg/<pkgname>/<pkgname>-installed-files
echo "-->> Preparing The Installation Of Sample"
cd /tmp
echo "-->> Cloning The Git Repo"
git clone <url>
cd <pkgname>
echo "-->> Compiling In fakeroot & Installing The Application"
qmake/cmake && fakeroot -- bash "make"
<commands to run to copy>
echo "-->> Packaging Sucessful.."

