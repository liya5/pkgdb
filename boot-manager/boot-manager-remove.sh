#!/bin/bash
pkgname=boot-manager
echo "-->> Removing The App"
rm -rf /usr/share/icons/hicolor/scalable/apps/$pkgname.png
rm -rf /usr/share/doc/$pkgname/help/$pkgname.html
rm -rf /usr/bin/$pkgname
rm -rf /usr/share/applications/$pkgname.desktop
cat /var/lmr/pkg.lst | sed 's/\boot-manager\b//g' > /var/lmr/pkg.lst
rm -rf /var/lmr/pkg/$pkgname
echo "-->> Removal Successful"
