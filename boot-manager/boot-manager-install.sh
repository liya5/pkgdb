#!/bin/bash
pkgname=boot-manager
insdir=/var/lmr/pkg/$pkgname/$pkgname-installed-files
echo "-->> Preparing The Installation Of Boot Options"
cd /tmp
echo "-->> Cloning The Git Repo"
git clone https://gitlab.com/liya5/generic-apps/$pkgname.git
cd $pkgname
echo "-->> Compiling In fakeroot & Installing The Application"
qmake && make
mkdir -p $insdir/usr/bin/ && cp -rf $pkgname /usr/bin/$pkgname
mkdir -p $insdir/usr/share/applications/ && cp -rf $pkgname.desktop $insdir/usr/share/applications/$pkgname.desktop
mkdir -p $insdir/usr/share/doc/$pkgname/help/ && cp -rf help/$pkgname.html $insdir/usr/share/doc/$pkgname/help/$pkgname.html
echo "-->> Packaging Sucessful.."

