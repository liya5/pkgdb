#!/bin/bash
pkgname=lmgr
insdir=/var/lmr/pkg/$pkgname/$pkgname-installed-files
echo "-->> Preparing The Installation Of Sample"
cd /tmp
echo "-->> Cloning The Git Repo"
git clone https://gitlab.com/liya5/$pkgname.git
cd $pkgname
echo "-->> Compiling In fakeroot & Installing The Application"
mkdir -p $insdir/usr/bin
cp -rf lmr-r $insdir/usr/bin
cp -rf lmr-i $insdir/usr/bin
cp -rf lmr-up $insdir/usr/bin
echo "-->> Packaging Sucessful.."

