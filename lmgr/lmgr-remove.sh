#!/bin/bash
pkgname=lmgr
echo "-->> Removing The App"
rm -rf /usr/bin/lmr-*
cat /var/lmr/pkg.lst | sed 's/\lmgr\b//g' > /var/lmr/pkg.lst
rm -rf /var/lmr/pkg/$pkgname
echo "-->> Removal Successful"
